package com.demo.demo

import com.demo.demo.helpers.FormatterProvider
import com.demo.demo.models.Rate
import com.demo.demo.ui.adapters.RatesAdapter
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.powermock.modules.junit4.PowerMockRunner
import org.powermock.reflect.Whitebox
import kotlin.test.assertTrue

@RunWith(PowerMockRunner::class)
class RatesAdapterTest {
    private lateinit var formatters: FormatterProvider
    private lateinit var adapter: RatesAdapter

    private lateinit var rates: ArrayList<Rate>
    private lateinit var selectedRate: String
    private var selectedRateValue: Double = 0.0

    @Before
    fun setUp() {
        formatters = FormatterProvider()
        adapter = RatesAdapter(formatters)

        rates = ArrayList<Rate>()
        rates.add(Rate("USD", "US Dollar", 25.2))
        rates.add(Rate("EUR", "EURO", 21.1))
        rates.add(Rate("RUR", "Rouble", 5.82))
        selectedRate = "USD"
        selectedRateValue = 25.2

        adapter.setData(rates, selectedRate, selectedRateValue)
    }

    @Test
    fun shouldInitializeCorrectly() {
        val adapter = RatesAdapter(formatters)
        adapter.setData(rates, selectedRate, selectedRateValue)

        assertTrue { Whitebox.getInternalState<String>(adapter, "selectedRate") == selectedRate }
        assertTrue { Whitebox.getInternalState<Double>(adapter, "selectedRateValue") == selectedRateValue }
        assertTrue { Whitebox.getInternalState<ArrayList<Rate>>(adapter, "rates") == rates }
    }

    @Test
    fun shouldHaveCorrectItemsCount() {
        assertTrue { adapter.itemCount == rates.count() }
    }
}