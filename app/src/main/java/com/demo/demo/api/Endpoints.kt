package com.demo.demo.api

import com.demo.demo.models.*
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface Endpoints {
    @GET("latest.json")
    fun getExchangeRates(@Query("base") base: String,
                         @Query("app_id") appId: String,
                         @Query("symbols") symbols: String? = null): Observable<RatesResponse>
}