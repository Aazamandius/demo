package com.demo.demo.api

import io.reactivex.Observable
import retrofit2.http.GET

interface CurrenciesEndpoints {
    @GET("currencies.json")
    fun getCurrencyCodes(): Observable<HashMap<String, String>>
}