package com.demo.demo.api

import com.google.gson.Gson
import com.google.gson.JsonSyntaxException
import com.google.gson.TypeAdapter
import com.google.gson.TypeAdapterFactory
import com.google.gson.internal.bind.util.ISO8601Utils
import com.google.gson.reflect.TypeToken
import com.google.gson.stream.JsonReader
import com.google.gson.stream.JsonToken
import com.google.gson.stream.JsonWriter
import java.io.IOException
import java.text.DateFormat
import java.text.ParseException
import java.text.ParsePosition
import java.text.SimpleDateFormat
import java.util.*

class DateTypeAdapter : TypeAdapter<Date>() {
    private val enUsFormat: DateFormat
    private val localFormat: DateFormat

    init {
        this.enUsFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.US)
        this.localFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.US)
        this.localFormat.setTimeZone(TimeZone.getTimeZone("UTC"))
    }

    @Throws(IOException::class)
    override fun read(`in`: JsonReader): Date? {
        if (`in`.peek() == JsonToken.NULL) {
            `in`.nextNull()
            return null
        } else {
            return this.deserializeToDate(`in`.nextString())
        }
    }

    @Synchronized
    private fun deserializeToDate(json: String): Date {
        if (json.length < 4 || json.substring(0, 4) == "0000") {
            return Date(0)
        }

        try {
            return this.localFormat.parse(json)
        } catch (var5: ParseException) {
            try {
                return this.enUsFormat.parse(json)
            } catch (var4: ParseException) {
                try {
                    return ISO8601Utils.parse(json, ParsePosition(0))
                } catch (var3: ParseException) {
                    throw JsonSyntaxException(json, var3)
                }

            }

        }

    }

    @Synchronized
    @Throws(IOException::class)
    override fun write(out: JsonWriter, value: Date?) {
        if (value == null) {
            out.nullValue()
        } else {
            val dateFormatAsString = this.enUsFormat.format(value)
            out.value(dateFormatAsString)
        }
    }

    companion object {
        val Factory: TypeAdapterFactory = object : TypeAdapterFactory {
            override fun <T> create(gson: Gson, typeToken: TypeToken<T>): TypeAdapter<T>? {
                return if (typeToken.rawType == Date::class.java) DateTypeAdapter() as TypeAdapter<T> else null
            }
        }
    }
}
