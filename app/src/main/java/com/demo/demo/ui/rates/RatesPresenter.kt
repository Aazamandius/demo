package com.demo.demo.ui.rates

import com.demo.demo.BuildConfig
import com.demo.demo.api.CurrenciesEndpoints
import com.demo.demo.api.Endpoints
import com.demo.demo.helpers.AppSchedulerProvider
import com.demo.demo.helpers.SpHelper
import com.demo.demo.models.Rate
import com.demo.demo.models.RatesResponse
import com.demo.demo.mvp.BasePresenter
import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import io.reactivex.functions.BiFunction
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import kotlin.collections.HashMap

class RatesPresenter
@Inject constructor(private val view: RatesPresenterContract.View,
                    private val api: Endpoints,
                    private val capi: CurrenciesEndpoints,
                    private val scheduler: AppSchedulerProvider,
                    private val spHelper: SpHelper) : BasePresenter<RatesPresenterContract.View>(view), RatesPresenterContract.Presenter {

    private var ratesUpdater: Disposable? = null

    override var rates: ArrayList<Rate>? = null

    override var selectedCurrency: String = spHelper.getDefaultCurrency()

    override var selectedCurrencyValue: Double = 100.0

    override var isLoading = false

    private fun updateSelectedCurrency(newValue: String, oldValue: String) {
        rates?.also {
            val oldrate = it.firstOrNull { c -> c.currency == oldValue }
            val rate = it.firstOrNull { c -> c.currency == newValue }
            val pos = it.indexOf(rate)

            val noRateValue = selectedCurrencyValue / (oldrate?.value ?: 1.0)
            spHelper.setDefaultCurrency(newValue)
            selectedCurrencyValue = rate?.value?.times(noRateValue) ?: selectedCurrencyValue

            if (rate != null) {
                it.remove(rate)
                it.add(0, rate)
            }

            view.moveToFirstPlace(pos)
        }
    }

    private fun startUpdateRates() {
        ratesUpdater?.dispose()
        ratesUpdater = Observable
                .interval(1, TimeUnit.SECONDS)
                .subscribeOn(scheduler.io())
                .observeOn(scheduler.ui())
                .subscribe {
                    reloadRates()
                }
    }

    private fun stopUpdateRates() {
        ratesUpdater?.dispose()
    }

    private fun updateRatesValues(oldRates: ArrayList<Rate>, newRates: List<Rate>): ArrayList<Rate> {
        val toAdd = ArrayList<Rate>()
        newRates.forEach { rate ->
            val exists = oldRates.firstOrNull { it.currency == rate.currency }
            if (exists != null) {
                exists.value = rate.value
            } else {
                toAdd.add(rate)
            }
        }
        oldRates.addAll(toAdd)

        val toRemove = oldRates.filterNot { r -> newRates.firstOrNull { rr -> rr.currency == r.currency} != null }
        oldRates.removeAll(toRemove)

        return oldRates
    }

    override fun selectCurrency(newRate: String) {
        if (newRate != selectedCurrency) {
            stopUpdateRates()

            updateSelectedCurrency(newRate, selectedCurrency)
            selectedCurrency = newRate

            startUpdateRates()
        }
    }

    override fun reloadRates() {

        if (isLoading) {
            return
        }
        isLoading = true

        disposeCurrentSubscriptions()

        if (rates == null) {
            view.showProgress(null)
        }

        // Base rates not for free plan
        addDisposable(Observable.zip(api.getExchangeRates("USD", BuildConfig.RATES_API_KEY), capi.getCurrencyCodes(),
                BiFunction<RatesResponse, HashMap<String, String>, List<Rate>> {r, c ->
                    prepareRates(r, c)
                })
                .doOnNext {
                    this.rates = updateRatesValues(this.rates ?: ArrayList(), it)
                }
                .subscribeOn(scheduler.io())
                .observeOn(scheduler.ui())
                .onErrorReturn {
                    println(it.toString())
                    return@onErrorReturn null
                }
                .doFinally {
                    isLoading = false
                    view.hideProgress()
                }
                .subscribe({
                    view.updateData()
                }, {
                    view.showErrorMessage(it.localizedMessage)
                }))
    }

    private fun prepareRates(ratesResponse: RatesResponse, codes: HashMap<String, String>): ArrayList<Rate> {
        val rates = ArrayList<Rate>()
        rates.addAll(ratesResponse.rates?.map { Rate(it.key, codes[it.key] ?: it.key, it.value) } ?: listOf())
        return rates
    }

    override fun onResume() {
        super.onResume()
        startUpdateRates()
    }

    override fun onPause() {
        super.onPause()
        stopUpdateRates()
    }
}