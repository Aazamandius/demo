package com.demo.demo.ui.adapters

import android.text.Editable
import android.text.TextWatcher
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.demo.demo.R
import com.demo.demo.helpers.FormatterProvider
import com.demo.demo.models.Rate
import java.text.ParseException

class RatesAdapter(private val formatters: FormatterProvider) : RecyclerView.Adapter<RecyclerView.ViewHolder>(), TextWatcher {

    private val CONTENT_VIEW = 0

    private var rates: ArrayList<Rate>? = null
    private var selectedRate: String? = null
    private var selectedRateValue: Double? = null

    private var clickListener: OnItemClickListener? = null
    private var valueListener: OnValueChangedListener? = null

    private var firstViewHolder: RatesViewHolder? = null

    fun setData(rates: ArrayList<Rate>?,
                selectedRate: String,
                selectedRateValue: Double) {

        this.rates = rates ?: ArrayList()
        this.selectedRate = selectedRate
        this.selectedRateValue = selectedRateValue
    }

    fun setOnItemClickListener(listener: OnItemClickListener) {
        clickListener = listener
    }

    fun setOnValueChangeListener(listener: OnValueChangedListener) {
        valueListener = listener
    }

    override fun getItemViewType(position: Int): Int {
        return CONTENT_VIEW
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.rates_view_layout, parent, false)
        val holder = RatesViewHolder(view, formatters)
        return holder
    }

    override fun getItemCount(): Int {
        return rates?.size ?: 0
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val rate = rates!![position]
        (holder as RatesViewHolder).apply {
            this.currencyValue.removeTextChangedListener(this@RatesAdapter)

            setData(this, rate)

            this.itemView.setOnClickListener {
                this.focusCurrencyValue()
                clickListener?.itemClick(rate)
            }
            this.currencyValue.setOnFocusChangeListener { _, hasFocus ->
                if (hasFocus) {
                    clickListener?.itemClick(rate)
                }
            }

            if (position == 0) {
                this.currencyValue.addTextChangedListener(this@RatesAdapter)
            }

            if (position == 0) {
                firstViewHolder = this
            }
        }
    }

    private fun setData(holder: RatesViewHolder, rate: Rate) {
        val selectedRate = rates!!.firstOrNull{ it.currency == selectedRate }
        val noRateValue = (selectedRateValue ?: 0.0) / (selectedRate?.value ?: 1.0)
        holder.setData(rate, noRateValue)
    }

    private fun changeValue(s: String?) {
        val rate = rates!![0]
        val value = try {
            formatters.dff.parse(s)?.toDouble() ?: 0.0
        } catch (e: ParseException) {
            0.0
        }
        valueListener?.valueChanged(rate, value)
    }

    override fun afterTextChanged(s: Editable?) {
        changeValue(s?.toString())
    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

    }

    interface OnItemClickListener {
        fun itemClick(item: Rate)
    }

    interface OnValueChangedListener {
        fun valueChanged(item: Rate, value: Double)
    }
}