package com.demo.demo.ui.adapters

import android.view.View
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.demo.demo.R
import com.demo.demo.extensions.createHexColor
import com.demo.demo.helpers.FormatterProvider
import com.demo.demo.models.Rate
import java.util.*

class RatesViewHolder(itemView: View, private val formatters: FormatterProvider) : RecyclerView.ViewHolder(itemView)
{
    private val currencyImage = itemView.findViewById<ImageView>(R.id.im_currency)
    private val currencyCode = itemView.findViewById<TextView>(R.id.tv_name)
    private val currencyName = itemView.findViewById<TextView>(R.id.tv_description)
    val currencyValue = itemView.findViewById<EditText>(R.id.et_value)

    private var rate: Rate? = null

    fun setData(rate: Rate, count: Double) {
        if (rate.currency != this.rate?.currency) {
            currencyCode.text = rate.currency
            currencyName.text = rate.currencyName
            setImage(rate)
        }

        this.rate = rate
        currencyValue.setText(formatters.dff.format(rate.value * count))
    }

    private fun setImage(rate: Rate) {
        val iconUrl = String.format(iconsUrl, rate.currency.toLowerCase(Locale.getDefault()))
        val placeholder = createHexColor(itemView.context, rate.currency)

        Glide.with(itemView)
                .load(iconUrl)
                .placeholder(placeholder)
                .apply(RequestOptions().circleCrop())
                .into(currencyImage)
    }

    fun focusCurrencyValue() {
        currencyValue.requestFocus()
    }

    companion object {
        const val iconsUrl = "https://raw.githubusercontent.com/cychiang/currency-icons/master/icons/currency/%s.png"
    }
}