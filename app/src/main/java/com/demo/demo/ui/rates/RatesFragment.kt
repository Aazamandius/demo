package com.demo.demo.ui.rates

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.demo.demo.R
import com.demo.demo.helpers.FormatterProvider
import com.demo.demo.models.Rate
import com.demo.demo.mvp.BaseFragment
import com.demo.demo.ui.adapters.RatesAdapter
import kotlinx.android.synthetic.main.fragment_rating.*
import javax.inject.Inject
import androidx.recyclerview.widget.RecyclerView

class RatesFragment : BaseFragment<RatesPresenterContract.Presenter>(RatesPresenterContract.Presenter::class.java), RatesPresenterContract.View {

    companion object {
        fun create(): RatesFragment {
            return RatesFragment()
        }
    }

    @Inject
    lateinit var formatters: FormatterProvider

    private lateinit var adapter: RatesAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_rating, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setTitle(R.string.rates_fragment_title)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        adapter = RatesAdapter(formatters)

        presenter.reloadRates()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        // Set the required LayoutManager
        val layoutManager = LinearLayoutManager(context)

        val recycledViewPool = RecyclerView.RecycledViewPool()
        // In this case too many holders recreation called
        recycledViewPool.setMaxRecycledViews(0, 32)
        recycler.setRecycledViewPool(recycledViewPool)
        recycler.layoutManager = layoutManager
        recycler.adapter = adapter

        val itemDecorator = DividerItemDecoration(context!!, DividerItemDecoration.VERTICAL)
        itemDecorator.setDrawable(ContextCompat.getDrawable(context!!, R.drawable.divider)!!)
        recycler.addItemDecoration(itemDecorator)

        adapter.setOnItemClickListener(object : RatesAdapter.OnItemClickListener {
            override fun itemClick(item: Rate) {
                selectRate(item)
            }
        })

        adapter.setOnValueChangeListener(object : RatesAdapter.OnValueChangedListener {
            override fun valueChanged(item: Rate, value: Double) {
                presenter.selectedCurrencyValue = value
                updateData()
            }
        })
    }

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)
        savedInstanceState?.getDouble("selectedCurrencyValue")?.let {
            presenter.selectedCurrencyValue = it
        }
        savedInstanceState?.remove("selectedCurrencyValue")
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putDouble("selectedCurrencyValue", presenter.selectedCurrencyValue)
    }

    private fun selectRate(item: Rate) {
        presenter.selectCurrency(item.currency)
    }

    override fun moveToFirstPlace(item: Int) {
        adapter.notifyItemMoved(item, 0)
        adapter.notifyItemChanged(0)
        recycler.scrollToPosition(0)
    }

    override fun updateData()  {
        if (!recycler.isComputingLayout) {
            adapter.setData(presenter.rates, presenter.selectedCurrency, presenter.selectedCurrencyValue)
            adapter.notifyItemRangeChanged(1, presenter.rates?.count()?.minus(1) ?: 0, presenter.rates)
        }
    }
}
