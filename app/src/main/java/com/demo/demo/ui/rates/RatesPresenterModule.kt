package com.demo.demo.ui.rates

import dagger.Binds
import dagger.Module

@Module
abstract class RatesPresenterModule {

    @Binds
    internal abstract fun ratesView(view: RatesFragment): RatesPresenterContract.View

    @Binds
    internal abstract fun ratesPresenter(presenter: RatesPresenter): RatesPresenterContract.Presenter
}