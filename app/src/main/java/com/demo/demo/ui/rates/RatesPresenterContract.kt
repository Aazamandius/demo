package com.demo.demo.ui.rates

import com.demo.demo.models.Rate
import com.demo.demo.mvp.CommonView
import com.demo.demo.mvp.ViewPresenter

interface RatesPresenterContract {

    interface View: CommonView {
        fun updateData()
        fun moveToFirstPlace(item: Int)
    }

    interface Presenter: ViewPresenter<View> {
        val isLoading: Boolean
        val rates: ArrayList<Rate>?
        val selectedCurrency: String
        var selectedCurrencyValue: Double

        fun reloadRates()
        fun selectCurrency(newRate: String)
    }
}