package com.demo.demo.ui.common

import android.content.Intent
import android.net.Uri
import androidx.fragment.app.FragmentManager
import com.demo.demo.R
import com.demo.demo.activities.BaseActivity
import com.demo.demo.ui.rates.RatesFragment
import javax.inject.Inject

class NavigationController
@Inject constructor(private val activity: BaseActivity) {
    private val containerId: Int = R.id.contentPanel
    private val fragmentManager: FragmentManager = activity.supportFragmentManager

    fun navigateToExchangeRates() {
        val ratingFragment = RatesFragment.create()
        fragmentManager.beginTransaction()
                .replace(containerId, ratingFragment)
                .addToBackStack(ratingFragment::class.java.name)
                .commitAllowingStateLoss()
    }

    fun navigateToExternalUrl(url: String) {
        val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
        activity.startActivity(browserIntent)
    }

    fun navigateToEmailComposer(email: String) {
        val emailIntent = Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto", email, null))
        activity.startActivity(Intent.createChooser(emailIntent, "Send Email"))
    }
}
