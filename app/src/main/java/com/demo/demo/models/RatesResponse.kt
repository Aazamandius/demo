package com.demo.demo.models

import java.util.*
import kotlin.collections.HashMap

data class RatesResponse(
        var base: String?,
        var date: Date?,
        var rates: HashMap<String, Double>?)