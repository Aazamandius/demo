package com.demo.demo.models

data class Rate(
        var currency: String,
        var currencyName: String,
        var value: Double)