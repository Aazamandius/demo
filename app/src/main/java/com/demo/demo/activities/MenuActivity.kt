package com.demo.demo.activities

interface MenuActivity {
    fun enableViews(enable: Boolean)
}