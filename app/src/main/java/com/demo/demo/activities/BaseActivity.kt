package com.demo.demo.activities

import android.app.Activity
import android.content.Context
import android.content.DialogInterface
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.annotation.StringRes
import androidx.core.content.ContextCompat
import com.demo.demo.R
import com.demo.demo.helpers.DialogHelper
import com.demo.demo.helpers.ProgressDialog
import com.demo.demo.helpers.SnackBarBuilder
import com.demo.demo.mvp.CommonView
import com.demo.demo.ui.common.NavigationController
import dagger.android.support.DaggerAppCompatActivity
import javax.inject.Inject

abstract class BaseActivity : DaggerAppCompatActivity(), CommonView {

    private var progressDialog: ProgressDialog? = null

    @Inject
    override lateinit var navigationController: NavigationController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            window.addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN)
            window.addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS)
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.navigationBarColor = ContextCompat.getColor(this, R.color.main_background);
        }
    }

    override fun showSnakeBar(targetView: View, @StringRes resID: Int) {
        showSnakeBar(targetView, getString(resID))
    }

    override fun showSnakeBar(targetView: View, text: String) {
        SnackBarBuilder(targetView, text).show()
    }

    override fun showInfoMessage(resID: Int) {
        showInfoMessage(getString(resID))
    }

    override fun showErrorMessage(resID: Int) {
        showErrorMessage(getString(resID))
    }

    override fun showErrorMessage(text: String) {
        //Get the root view
        showInfoMessage(text)
    }

    override fun showInfoMessage(text: String) {
        //Get the root view
        val view = findViewById<ViewGroup>(android.R.id.content)
        if (view != null) {
            val rootView = view.rootView
            if (rootView != null) {
                //Show SnakeBar
                showSnakeBar(rootView, text)

                //Hide keyboard
                hideSoftKeyboard()

                return
            }
        }

        //Show toast
        showToast(text)
    }

    private fun showToast(text: String) {
        Toast.makeText(this, text, Toast.LENGTH_LONG).show()
    }

    override fun showProgress(onCancelListener: DialogInterface.OnCancelListener?) {
        if (progressDialog == null || !progressDialog!!.isShowing) {
            progressDialog = DialogHelper.getProgressDialog(this, onCancelListener)
            progressDialog?.show()
        }
    }

    override fun hideProgress() {
        progressDialog?.dismiss()
    }

    override fun hideSoftKeyboard() {
        try {
            val inputMethodManager = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.hideSoftInputFromWindow(currentFocus?.windowToken, 0)
        } catch (e: Exception) {
            Log.e(this.javaClass.name + ".hideSoftKeyboard()", e.toString())
        }

    }

    override fun showSoftKeyboard() {
        try {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, InputMethodManager.HIDE_IMPLICIT_ONLY)
        } catch (e: Exception) {
            Log.e(this.javaClass.name + "," + "showSoftKeyboard()", e.toString())
        }

    }

    override fun close() {
        finish()
    }
}
