package com.demo.demo.activities

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import com.demo.demo.R
import com.demo.demo.extensions.setupTestSwitch
import com.demo.demo.helpers.SpHelper
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import javax.inject.Inject

class MainActivity : BaseActivity(), MenuActivity {

    @Inject
    lateinit var spHelper: SpHelper

    private var drawerToggle: ActionBarDrawerToggle? = null
    private var toolBarNavigationListenerIsRegistered = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (savedInstanceState == null) {
            navigationController.navigateToExchangeRates()
        }

        setSupportActionBar(toolbar)

        drawerToggle = object : ActionBarDrawerToggle(
                this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {

            override fun onDrawerSlide(drawerView: View, slideOffset: Float) {
                super.onDrawerSlide(drawerView, slideOffset)
                val moveFactor = drawerView.width * slideOffset
                view_drawer_container.translationX = moveFactor
            }

        }
        drawerToggle!!.isDrawerIndicatorEnabled = true
        drawerLayout.addDrawerListener(drawerToggle!!)

        drawerToggle!!.syncState()

        setupTestSwitch(spHelper)

        hideMenu()
    }

    /**
     * To be semantically or contextually correct, maybe change the name
     * and signature of this function to something like:
     *
     * private void showBackButton(boolean show)
     * Just a suggestion.
     */
    override fun enableViews(enable: Boolean) {

        // To keep states of ActionBar and ActionBarDrawerToggle synchronized,
        // when you enable on one, you disable on the other.
        // And as you may notice, the order for this operation is disable first, then enable - VERY VERY IMPORTANT.
        if (enable) {
            //You may not want to open the drawer on swipe from the left in this case
            drawerLayout.setDrawerLockMode(androidx.drawerlayout.widget.DrawerLayout.LOCK_MODE_LOCKED_CLOSED)
            // Remove hamburger
            drawerToggle?.isDrawerIndicatorEnabled = false
            // Show back button
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
            // when DrawerToggle is disabled i.e. setDrawerIndicatorEnabled(false), navigation icon
            // clicks are disabled i.e. the UP button will not work.
            // We need to add a listener, as in below, so DrawerToggle will forward
            // click events to this listener.
            if (!toolBarNavigationListenerIsRegistered) {
                drawerToggle?.toolbarNavigationClickListener = View.OnClickListener {
                    // Doesn't have to be onBackPressed
                    onBackPressed()
                }

                toolBarNavigationListenerIsRegistered = true
            }

        } else {
            //You must regain the power of swipe for the drawer.
            drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED)

            // Remove back button
            supportActionBar!!.setDisplayHomeAsUpEnabled(false)
            // Show hamburger
            drawerToggle?.isDrawerIndicatorEnabled = true
            // Remove the/any drawer toggle listener
            drawerToggle?.toolbarNavigationClickListener = null
            toolBarNavigationListenerIsRegistered = false
        }

        // So, one may think "Hmm why not simplify to:
        // .....
        // getSupportActionBar().setDisplayHomeAsUpEnabled(enable);
        // mDrawer.setDrawerIndicatorEnabled(!enable);
        // ......
        // To re-iterate, the order in which you enable and disable views IS important #dontSimplify.
    }

    private fun showMenu() {
        drawerLayout.openDrawer(GravityCompat.START, true)
    }

    private fun hideMenu() {
        drawerLayout.closeDrawers()
    }

    override fun onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            hideMenu()
        } else {
            super.onBackPressed()
        }
    }
}
