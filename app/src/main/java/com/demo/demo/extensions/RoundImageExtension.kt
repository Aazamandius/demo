package com.demo.demo.extensions

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.BitmapDrawable
import com.demo.demo.R
import com.demo.demo.helpers.TextInCircleGenerator
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException

fun createHexColor(context: Context, account: String): BitmapDrawable {
    val hex = "#" + (md5(account)?.substring(0, 6) ?: "cccccc")
    val color = Color.parseColor(hex)

    val circleGenerator = TextInCircleGenerator.Builder
            .setDiameter(context.resources.getDimension(R.dimen.rates_icon_size).toInt())
            .setCircleColor(color)
            .setTextColor(Color.parseColor("#ffffff"))
            .build()
    return circleGenerator.generate(context.resources, if(account.length > 2) account.substring(0, 3) else account)
}

fun md5(input: String): String? {
    try {
        val md = MessageDigest.getInstance("MD5")

        val hexString = StringBuilder()
        for (digestByte in md.digest(input.toByteArray()))
            hexString.append(String.format("%02X", digestByte))

        return hexString.toString()
    } catch (e: NoSuchAlgorithmException) {
        e.printStackTrace()
        return null
    }

}