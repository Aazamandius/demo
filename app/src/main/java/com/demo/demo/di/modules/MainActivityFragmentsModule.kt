package com.demo.demo.di.modules

import com.demo.demo.activities.BaseActivity
import com.demo.demo.activities.MainActivity
import com.demo.demo.ui.rates.RatesFragment
import com.demo.demo.ui.rates.RatesPresenterModule
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class MainActivityFragmentsModule {
    @Binds
    abstract fun provideContext(activity: MainActivity): BaseActivity

    @ContributesAndroidInjector(modules = [RatesPresenterModule::class])
    abstract fun contributeRatesFragment(): RatesFragment
}
