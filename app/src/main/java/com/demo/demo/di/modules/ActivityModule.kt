package com.demo.demo.di.modules

import com.demo.demo.activities.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityModule {
    @ContributesAndroidInjector(modules = [MainActivityFragmentsModule::class])
    internal abstract fun contributeMainActivity(): MainActivity
}
