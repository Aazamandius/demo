package com.demo.demo.di.modules

import android.app.Application
import com.demo.demo.BuildConfig
import com.demo.demo.helpers.LoggingInterceptor
import dagger.Module
import dagger.Provides
import okhttp3.*
import java.util.concurrent.TimeUnit
import javax.inject.Named
import javax.inject.Singleton

@Module
class OkHttpModule {
    private fun getBaseBuilder(): OkHttpClient.Builder {
        return OkHttpClient.Builder()
                .retryOnConnectionFailure(true)
                .connectTimeout(120, TimeUnit.SECONDS)
                .readTimeout(120, TimeUnit.SECONDS)
                .writeTimeout(120, TimeUnit.SECONDS)
    }

    private class CachingControlInterceptor : Interceptor {

        override fun intercept(chain: Interceptor.Chain): Response {
            val response = chain.proceed(chain.request())
            val cacheControl = CacheControl.Builder()
                    .maxAge( 24, TimeUnit.HOURS )
                    .build()

            return response.newBuilder()
                    .header("Cache-Control", cacheControl.toString() )
                    .build()

        }
    }

    @Provides
    @Singleton
    fun providesOkHttpCache(pApplication: Application): Cache =
            Cache(pApplication.cacheDir, 10 * 1024 * 1024)

    @Provides
    @Singleton
    fun providesLoggingInterceptor() : LoggingInterceptor {
        return LoggingInterceptor()
    }

    @Provides
    @Singleton
    @Named("rates")
    fun providesOkHttpRates(cache: Cache, loggingInterceptor: LoggingInterceptor) : OkHttpClient {
        val builder = getBaseBuilder()

        if (BuildConfig.DEBUG) {
            builder.interceptors().add(loggingInterceptor)
        }

        return builder.build()
    }

    @Provides
    @Singleton
    @Named("currencies")
    fun providesOkHttpCurrencies(cache: Cache, loggingInterceptor: LoggingInterceptor) : OkHttpClient {
        val builder = getBaseBuilder()

        builder.addNetworkInterceptor(CachingControlInterceptor())

        builder.cache(cache)

//        if (BuildConfig.DEBUG) {
//            builder.interceptors().add(LoggingInterceptor())
//        }

        return builder.build()
    }
}