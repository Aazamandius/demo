package com.demo.demo.di.modules

import android.app.Application
import android.content.Context
import com.demo.demo.App
import com.demo.demo.api.DateTypeAdapter
import com.demo.demo.helpers.AppSchedulerProvider
import com.demo.demo.helpers.SpHelper
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Singleton

@Module(includes = [
    ApiModule::class,
    RetrofitModule::class,
    OkHttpModule::class,
    FormatterModule::class])
class AppModule {
    @Provides
    @Singleton
    fun provideContext(application: App): Context {
        return application
    }

    @Provides
    @Singleton
    fun provideApplication(application: App): Application {
        return application
    }

    @Provides
    @Singleton
    fun providesGson(): Gson {
        val gsonBuilder = GsonBuilder()
        gsonBuilder.setLenient()

        gsonBuilder.registerTypeAdapterFactory(DateTypeAdapter.Factory)

        return gsonBuilder.create()
    }

    @Provides
    @Singleton
    fun providesSharedPref(gson: Gson, context: Context) = SpHelper(context.getSharedPreferences("Sp", Context.MODE_PRIVATE), gson)

    @Provides
    @Singleton
    fun provideCompositeDisposable() = CompositeDisposable()

    @Provides
    @Singleton
    fun provideSchedulerProvider() = AppSchedulerProvider()
}
