package com.demo.demo.di.modules

import com.demo.demo.helpers.FormatterProvider
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class FormatterModule {
    @Provides
    @Singleton
    fun providesFormatter(): FormatterProvider = FormatterProvider()
}