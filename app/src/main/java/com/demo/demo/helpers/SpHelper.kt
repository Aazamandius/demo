package com.demo.demo.helpers

import android.content.SharedPreferences
import com.google.gson.Gson

/**
 * Created by aazamandius on 03/12/2017.
 */
class SpHelper constructor(var sharedPreferences: SharedPreferences, var gson: Gson) {

    companion object {

        const val DefaultCurrency = "USD"

        const val SETTINGS_FAKE_API = "usefakeapi"
        const val SETTINGS_DEFAULT_CURRENCY = "settingsdefaultcurrency"
    }

    private var editor: SharedPreferences.Editor = sharedPreferences.edit()

    @Synchronized
    fun setFakeApi(isFake: Boolean) {
        editor.putBoolean(SETTINGS_FAKE_API, isFake)
        editor.commit()
    }

    @Synchronized
    fun getFakeApi(): Boolean {
        return sharedPreferences.getBoolean(SETTINGS_FAKE_API, false)
    }

    @Synchronized
    fun setDefaultCurrency(currency: String) {
        editor.putString(SETTINGS_DEFAULT_CURRENCY, currency)
        editor.commit()
    }

    @Synchronized
    fun getDefaultCurrency(): String {
        return sharedPreferences.getString(SETTINGS_DEFAULT_CURRENCY, DefaultCurrency)!!
    }
}