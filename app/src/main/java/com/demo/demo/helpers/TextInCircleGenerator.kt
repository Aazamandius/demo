package com.demo.demo.helpers

import android.content.Context
import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.drawable.BitmapDrawable
import androidx.annotation.ColorRes
import androidx.annotation.DimenRes
import androidx.core.content.ContextCompat

class TextInCircleGenerator {

    private val maxCharactersCount = 3
    private var diameter = 100
    private var textSize = diameter / 2.6f
    private var circleColor = Color.BLUE
    private var textColor = Color.BLACK

    fun generate(resources: Resources, text: String): BitmapDrawable {
        val bitmap = Bitmap.createBitmap(diameter, diameter, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)

        drawCircle(canvas)
        drawText(canvas, simplify(text))

        return BitmapDrawable(resources, bitmap)
    }

    fun simplify(text: String): String {
        val builder = StringBuilder()
        for (st in text) {
            if (builder.length >= maxCharactersCount)
                break
            builder.append(st)
        }
        return builder.toString().toUpperCase()
    }

    private fun drawCircle(canvas: Canvas) {
        val paint = Paint()
        paint.color = circleColor
        paint.flags = Paint.ANTI_ALIAS_FLAG

        val x = diameter / 2.0f
        val y = x
        val r = x
        canvas.drawCircle(x, y, r, paint)
    }

    private fun drawText(canvas: Canvas, text: String) {
        val paint = Paint()
        paint.color = textColor
        paint.textSize = textSize
        paint.flags = Paint.ANTI_ALIAS_FLAG
        paint.textAlign = Paint.Align.CENTER

        val x = diameter / 2.0f
        val y = ((diameter / 2) - ((paint.descent() + paint.ascent()) / 2))
        canvas.drawText(text, x, y, paint)
    }

    object Builder {
        private var generator: TextInCircleGenerator = TextInCircleGenerator()

        fun setDiameter(context: Context, @DimenRes diameter: Int): Builder = apply { generator.diameter = context.resources.getDimensionPixelSize(diameter) }

        fun setDiameter(diameterInPixels: Int): Builder = apply { generator.diameter = diameterInPixels }

        fun setCircleColor(context: Context, @ColorRes circleColor: Int): Builder = apply { generator.circleColor = ContextCompat.getColor(context, circleColor) }

        fun setTextColor(context: Context, @ColorRes textColor: Int): Builder = apply { generator.textColor = ContextCompat.getColor(context, textColor) }

        fun setCircleColor(circleColor: Int): Builder = apply { generator.circleColor = circleColor }

        fun setTextColor(textColor: Int): Builder = apply { generator.textColor = textColor }

        fun setTextSize(textSize: Float): Builder = apply { generator.textSize = textSize }

        fun build(): TextInCircleGenerator = generator
    }
}