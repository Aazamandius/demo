package ru.pfizer.surgeons.helpers

import io.reactivex.Scheduler

/**
 * Created by aazamandius on 03/12/2017.
 */
interface SchedulerProvider {
    fun ui(): Scheduler
    fun computation(): Scheduler
    fun io(): Scheduler
}