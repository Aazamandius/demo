package com.demo.demo.helpers

import java.text.DecimalFormat
import java.text.DecimalFormatSymbols

class FormatterProvider {
    val dff: DecimalFormat //exactly 2 digits point valu

    init {
        val formatSymbols = DecimalFormatSymbols()
        formatSymbols.groupingSeparator = ' '
        formatSymbols.decimalSeparator = '.'

        dff = DecimalFormat("###,###,###,###,###,##0.00")
        dff.decimalFormatSymbols = formatSymbols
        dff.maximumFractionDigits = 2
        dff.groupingSize = 3
    }
}