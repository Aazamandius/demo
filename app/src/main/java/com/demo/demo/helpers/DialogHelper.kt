package com.demo.demo.helpers

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import androidx.annotation.StringRes
import androidx.appcompat.app.AlertDialog
import com.demo.demo.R

object DialogHelper {

    private val TAG = "DialogHelper"

    fun getProgressDialog(context: Context): ProgressDialog {
        val progressDialog = getProgressDialog(context, null)
        progressDialog.setCancelable(false)
        return progressDialog
    }

    fun getProgressDialog(context: Context, onCancelListener: DialogInterface.OnCancelListener?): ProgressDialog {
        return getProgressDialog(context, -1, onCancelListener)
    }

    @SuppressLint("ResourceType")
    fun getProgressDialog(context: Context, @StringRes messageId: Int, onCancelListener: DialogInterface.OnCancelListener?): ProgressDialog {
        val progressDialog = ProgressDialog(context)
        progressDialog.setMessage(context.getString(if (messageId > 0) messageId else R.string.please_wait))
        progressDialog.setOnCancelListener(onCancelListener)
        progressDialog.setCanceledOnTouchOutside(false)
        return progressDialog
    }

    fun showInfoDialog(context: Context, title: String?, message: String) {
        val builder = AlertDialog.Builder(context)
        builder.setTitle(title)
        builder.setMessage(message)
        val dialog = builder.create()
        dialog.setCanceledOnTouchOutside(true)
        dialog.show()
    }

    @SuppressLint("ResourceType")
    fun showInfoDialog(context: Context, @StringRes titleResID: Int, @StringRes messageResID: Int) {
        showInfoDialog(context, if (titleResID > 0) context.getString(titleResID) else null,
                context.getString(messageResID))
    }

    fun showOkDialog(context: Context, title: String, message: String, listener: DialogInterface.OnClickListener): Dialog {
        return showOkDialog(context, title, message, true, listener)
    }

    fun showOkDialog(context: Context,
                     title: String?,
                     message: String,
                     cancelable: Boolean,
                     listener: DialogInterface.OnClickListener): Dialog {

        val builder = AlertDialog.Builder(context)
        builder.setTitle(title)
        builder.setMessage(message)
        builder.setPositiveButton(R.string.ok, listener)
        builder.setCancelable(cancelable)
        val dialog = builder.create()
        dialog.show()
        return dialog
    }

    @SuppressLint("ResourceType")
    fun showOkCancelDialog(context: Context,
                           title: String?,
                           message: String,
                           onOkListener: DialogInterface.OnClickListener,
                           onCancelListener: DialogInterface.OnClickListener) {
        val builder = AlertDialog.Builder(context)
        if (title != null && !title.isEmpty()) {
            builder.setTitle(title)
        }
        builder.setMessage(message)
        builder.setPositiveButton(R.string.ok, onOkListener)
        builder.setNegativeButton(R.string.cancel, onCancelListener)

        val dialog = builder.create()
        dialog.show()
    }

    @SuppressLint("ResourceType")
    fun showOkCancelDialog(context: Context,
                           @StringRes title: Int,
                           @StringRes message: Int,
                           onOkListener: DialogInterface.OnClickListener,
                           onCancelListener: DialogInterface.OnClickListener) {
        val builder = AlertDialog.Builder(context)
        if (title > 0) {
            builder.setTitle(title)
        }
        builder.setMessage(message)
        builder.setPositiveButton(R.string.ok, onOkListener)
        builder.setNegativeButton(R.string.cancel, onCancelListener)

        val dialog = builder.create()
        dialog.show()
    }

    fun showOkDialog(context: Context,
                     @StringRes titleResID: Int,
                     @StringRes messageResID: Int,
                     listener: DialogInterface.OnClickListener): Dialog {

        return showOkDialog(context, titleResID, messageResID, true, listener)
    }

    @SuppressLint("ResourceType")
    fun showOkDialog(context: Context,
                     @StringRes titleResID: Int,
                     @StringRes messageResID: Int,
                     cancelable: Boolean,
                     listener: DialogInterface.OnClickListener): Dialog {

        return showOkDialog(context,
                if (titleResID > 0) context.getString(titleResID) else null,
                context.getString(messageResID),
                cancelable,
                listener)
    }

    fun showYesNoDialog(context: Context, title: String?, message: String,
                        onYesListener: DialogInterface.OnClickListener, onNoListener: DialogInterface.OnClickListener,
                        onCancelListener: DialogInterface.OnCancelListener?) {
        val builder = AlertDialog.Builder(context)
        builder.setTitle(title)
        builder.setMessage(message)
        builder.setPositiveButton(R.string.yes, onYesListener)
        builder.setNegativeButton(R.string.no, onNoListener)
        val dialog = builder.create()
        if (onCancelListener != null) {
            dialog.setOnCancelListener(onCancelListener)
            dialog.setCanceledOnTouchOutside(false)
        }
        dialog.show()
    }

    @SuppressLint("ResourceType")
    fun showYesNoDialog(context: Context, @StringRes titleResID: Int, @StringRes messageResID: Int,
                        onYesListener: DialogInterface.OnClickListener, onNoListener: DialogInterface.OnClickListener) {
        showYesNoDialog(context, if (titleResID > 0) context.getString(titleResID) else null,
                context.getString(messageResID), onYesListener, onNoListener, null)
    }

    fun showYesNoDialog(context: Context, titleResID: Int, messageResID: Int,
                        onYesListener: DialogInterface.OnClickListener, onNoListener: DialogInterface.OnClickListener,
                        onCancelListener: DialogInterface.OnCancelListener) {
        showYesNoDialog(context, if (titleResID > 0) context.getString(titleResID) else null,
                context.getString(messageResID), onYesListener, onNoListener, onCancelListener)
    }

    @SuppressLint("ResourceType")
    fun showCancelCustomDialog(context: Context, @StringRes titleResID: Int, @StringRes messageResID: Int, @StringRes customResID: Int,
                               onCustomListener: DialogInterface.OnClickListener) {
        showCancelCustomDialog(context, titleResID, messageResID,
                customResID, onCustomListener,
                R.string.cancel, DialogInterface.OnClickListener { dialog, _ -> dialog.dismiss() })
    }

    @SuppressLint("ResourceType")
    fun showCancelCustomDialog(context: Context, @StringRes titleResID: Int, @StringRes messageResID: Int,
                               @StringRes customOkResID: Int, onCustomOkListener: DialogInterface.OnClickListener,
                               @StringRes customCancelResID: Int, onCustomCancelListener: DialogInterface.OnClickListener) {
        val builder = AlertDialog.Builder(context)
        if (titleResID > 0) {
            builder.setTitle(titleResID)
        }
        builder.setMessage(messageResID)
        builder.setPositiveButton(customOkResID, onCustomOkListener)
        builder.setNegativeButton(customCancelResID, onCustomCancelListener)

        builder.create().show()
    }
}
