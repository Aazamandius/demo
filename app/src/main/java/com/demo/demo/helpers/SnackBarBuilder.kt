package com.demo.demo.helpers

import com.google.android.material.snackbar.Snackbar
import androidx.core.content.ContextCompat
import android.view.View
import com.demo.demo.R

/**
 * Display messages in SnackBar
 * @param targetView view to display
 * @param message Message text
 */
class SnackBarBuilder(targetView : View, message : String) {

    /**
     * SnackBar
     */
    private val snackBar : Snackbar = Snackbar.make(targetView, message, Snackbar.LENGTH_INDEFINITE)

    /**
     * Create SnackBar
     */
    init {
        //Set padding to view
        snackBar.view.setPadding(
                snackBar.view.paddingLeft,
                snackBar.view.paddingTop,
                snackBar.view.paddingRight,
                targetView.resources.getDimension(R.dimen.padding_snackbar).toInt()
        )

        //Action text color
        snackBar.setActionTextColor(ContextCompat.getColor(targetView.context, R.color.colorAccent))

        //Close button
        snackBar.setAction(R.string.snackbar_close) { snackBar.dismiss() }
    }

    /**
     * Display messages
     */
    fun show() {
        snackBar.show()
    }
}