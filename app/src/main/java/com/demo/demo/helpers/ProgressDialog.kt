package com.demo.demo.helpers

import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import android.widget.ProgressBar
import android.widget.TextView
import com.demo.demo.R

class ProgressDialog : AlertDialog {
    private var mProgress: ProgressBar? = null
    private var mMessageView: TextView? = null
    private var mMessageText: CharSequence? = null

    constructor(context: Context) : super(context) {}

    constructor(context: Context, themeResId: Int) : super(context, themeResId) {}

    constructor(context: Context, cancelable: Boolean, cancelListener: DialogInterface.OnCancelListener?) : super(context, cancelable, cancelListener) {}

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.progress_dialog)
        mProgress = findViewById(R.id.progress)
        mMessageView = findViewById(R.id.message)
        if (mMessageText != null) {
            mMessageView!!.text = mMessageText
        }
    }

    override fun setMessage(message: CharSequence) {
        if (mMessageView != null) {
            mMessageView!!.text = message
        } else {
            mMessageText = message
        }
    }
}
