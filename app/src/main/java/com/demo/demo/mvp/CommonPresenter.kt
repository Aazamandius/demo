package com.demo.demo.mvp

import android.app.Activity
import io.reactivex.disposables.Disposable

interface CommonPresenter {
    fun onViewCreated()

    fun onDestroyView()

    fun onResume()

    fun onPause()

    /**
     * The same as [Activity.onBackPressed]
     *
     * @return true, if action handled. false - if super method [Activity.onBackPressed] should be invoked
     */
    fun onBackPressed(): Boolean

    fun addDisposable(d: Disposable)

    fun disposeCurrentSubscriptions()
}