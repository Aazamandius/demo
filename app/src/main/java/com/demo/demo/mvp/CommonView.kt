package com.demo.demo.mvp

import android.content.DialogInterface
import androidx.annotation.StringRes
import android.view.View
import com.demo.demo.ui.common.NavigationController

interface CommonView {

    val navigationController: NavigationController

    fun close()

    fun showProgress(onCancelListener: DialogInterface.OnCancelListener?)

    fun hideProgress()

    fun showSnakeBar(targetView: View, @StringRes resID: Int)

    fun showSnakeBar(targetView: View, text: String)

    fun showInfoMessage(@StringRes resID: Int)

    fun showInfoMessage(text: String)

    fun showErrorMessage(@StringRes resID: Int)

    fun showErrorMessage(text: String)

    fun hideSoftKeyboard()

    fun showSoftKeyboard()

    fun setTitle(@StringRes title: Int)

    fun setTitle(title: CharSequence)
}