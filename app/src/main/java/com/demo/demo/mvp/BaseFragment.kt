package com.demo.demo.mvp

import android.app.Activity
import android.content.Context
import android.content.DialogInterface
import androidx.annotation.StringRes
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import com.demo.demo.helpers.DialogHelper
import com.demo.demo.helpers.ProgressDialog
import com.demo.demo.helpers.SnackBarBuilder
import com.demo.demo.ui.common.NavigationController
import dagger.android.support.DaggerFragment
import javax.inject.Inject

abstract class BaseFragment<P : CommonPresenter>(private val pClass: Class<P>) : DaggerFragment(), CommonView {

    @Inject
    override lateinit var navigationController: NavigationController

    @Inject
    lateinit var presenter: P

    private var progressDialog: ProgressDialog? = null

    override fun setTitle(title: CharSequence) {
        activity?.title = title
    }

    override fun setTitle(@StringRes title: Int) {
        activity?.setTitle(title)
    }

    override fun showSnakeBar(targetView: View, @StringRes resID: Int) {
        showSnakeBar(targetView, getString(resID))
    }

    override fun showSnakeBar(targetView: View, text: String) {
        SnackBarBuilder(targetView, text).show()
    }

    override fun showInfoMessage(resID: Int) {
        showInfoMessage(getString(resID))
    }

    override fun showErrorMessage(resID: Int) {
        showErrorMessage(getString(resID))
    }

    override fun showErrorMessage(text: String) {
        //Get the root view
        showInfoMessage(text)
    }

    override fun showInfoMessage(text: String) {
        //If rootView exists
        if (activity != null && activity is CommonView) {
            (activity as CommonView).showInfoMessage(text)
        } else {
            showToast(text)
        }
    }

    private fun showToast(text: String) {
        val context = context ?: return
        Toast.makeText(context, text, Toast.LENGTH_LONG).show()
    }

    override fun showProgress(onCancelListener: DialogInterface.OnCancelListener?) {
        val context = context ?: return

        if (progressDialog == null || !progressDialog!!.isShowing) {
            progressDialog = DialogHelper.getProgressDialog(context, onCancelListener)
            progressDialog?.show()
        }
    }

    override fun hideProgress() {
        progressDialog?.dismiss()
    }

    override fun hideSoftKeyboard() {
        try {
            val inputMethodManager = this.activity?.getSystemService(Activity.INPUT_METHOD_SERVICE) as? InputMethodManager
            inputMethodManager?.hideSoftInputFromWindow(this.activity?.currentFocus?.windowToken, 0)
        } catch (e: Exception) {
            Log.e(this.javaClass.name + ".hideSoftKeyboard()", e.toString())
        }
    }

    override fun showSoftKeyboard() {
        try {
            val imm = context?.getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager
            imm?.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, InputMethodManager.HIDE_IMPLICIT_ONLY)
        } catch (e: Exception) {
            Log.e(this.javaClass.name + "," + "showSoftKeyboard()", e.toString())
        }

    }

    override fun close() {
        popBackStack()
    }

    override fun onPause() {
        super.onPause()
        presenter.onPause()
    }

    override fun onResume() {
        super.onResume()
        presenter.onResume()
    }

    fun popBackStack() {
        if (fragmentManager?.backStackEntryCount == 0) {
            activity?.finish()
        } else {
            fragmentManager?.popBackStack()
        }
    }
}