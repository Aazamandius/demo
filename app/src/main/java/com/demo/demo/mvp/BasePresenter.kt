package com.demo.demo.mvp

import androidx.annotation.CallSuper
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

open class BasePresenter<V :  CommonView>(private val _view: V) : ViewPresenter<V> {

    private var compositeDisposable = CompositeDisposable()

    @CallSuper
    override fun onViewCreated() {
    }

    @CallSuper
    override fun onDestroyView() {
        compositeDisposable.dispose()
    }

    @CallSuper
    override fun onResume() {
    }

    @CallSuper
    override fun onPause() {
    }

    override fun onBackPressed(): Boolean {
        return false
    }

    override fun addDisposable(d: Disposable) {
        compositeDisposable.add(d)
    }

    override fun disposeCurrentSubscriptions() {
        compositeDisposable.dispose()
        compositeDisposable = CompositeDisposable()
    }
}