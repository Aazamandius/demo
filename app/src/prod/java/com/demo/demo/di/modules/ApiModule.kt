package com.demo.demo.di.modules

import com.demo.demo.api.CurrenciesEndpoints
import com.demo.demo.api.Endpoints
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Named
import javax.inject.Singleton

@Module
class ApiModule {
    @Provides
    @Singleton
    fun providesEndpoints(@Named("rates") retrofit: Retrofit): Endpoints {
        return retrofit.create(Endpoints::class.java)
    }

    @Provides
    @Singleton
    fun providesCurrenciesEndpoints(@Named("currencies") retrofit: Retrofit): CurrenciesEndpoints {
        return retrofit.create(CurrenciesEndpoints::class.java)
    }
}