package com.demo.demo.di.modules

import android.content.Context
import com.demo.demo.api.Endpoints
import com.demo.demo.api.FakeEndpoints
import com.demo.demo.helpers.SpHelper
import com.google.gson.Gson
import com.demo.demo.api.CurrenciesEndpoints
import com.demo.demo.api.FakeCurrenciesEndpoints
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Named
import javax.inject.Singleton

@Module
class ApiModule {
    @Provides
    @Singleton
    fun providesEndpoints(@Named("rates") retrofit: Retrofit,
                          spHelper: SpHelper,
                          context: Context,
                          gson: Gson): Endpoints {
        return if (spHelper.getFakeApi()) {
            FakeEndpoints(context, gson)
        } else {
            retrofit.create(Endpoints::class.java)
        }
    }

    @Provides
    @Singleton
    fun providesCurrenciesEndpoints(@Named("currencies") retrofit: Retrofit,
                          spHelper: SpHelper,
                          context: Context,
                          gson: Gson): CurrenciesEndpoints {
        return if (spHelper.getFakeApi()) {
            FakeCurrenciesEndpoints(context, gson)
        } else {
            retrofit.create(CurrenciesEndpoints::class.java)
        }
    }
}