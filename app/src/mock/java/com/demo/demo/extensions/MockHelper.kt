package com.demo.demo.extensions

import android.content.DialogInterface
import androidx.appcompat.widget.SwitchCompat
import com.demo.demo.R
import com.demo.demo.activities.MainActivity
import com.demo.demo.helpers.DialogHelper
import com.demo.demo.helpers.SpHelper

fun MainActivity.setupTestSwitch(spHelper: SpHelper) {
    val switch = findViewById<SwitchCompat>(R.id.test_switch)
    if (switch != null){
        switch.isChecked = spHelper.getFakeApi()
        switch.setOnCheckedChangeListener { _, _ ->
            spHelper.setFakeApi(switch.isChecked)

            DialogHelper.showOkDialog(
                    this,
                    R.string.warning_title,
                    R.string.restart_app_message,
                    true,
                    DialogInterface.OnClickListener { _, _ -> })
        }
    }
}