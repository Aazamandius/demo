package com.demo.demo.api

import android.content.Context
import com.demo.demo.R
import com.google.gson.Gson
import io.reactivex.Observable
import java.util.concurrent.TimeUnit

class FakeCurrenciesEndpoints(private val context: Context, private val gson: Gson) : CurrenciesEndpoints {
    override fun getCurrencyCodes(): Observable<HashMap<String, String>> {

        val text = context.resources.openRawResource(R.raw.exchange_codes_response)
                .bufferedReader().use { it.readText() }

        val response = gson.fromJson<HashMap<String, String>>(text, HashMap::class.java)

        return Observable.just(response).delay(1, TimeUnit.SECONDS)
    }
}