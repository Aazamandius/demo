package com.demo.demo.api

import android.content.Context
import com.demo.demo.R
import com.demo.demo.models.*
import com.google.gson.Gson
import io.reactivex.Observable
import retrofit2.http.Query
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.random.Random

class FakeEndpoints(private val context: Context, private val gson: Gson) : Endpoints {
    override fun getExchangeRates(@Query("base") base: String,
                                  @Query("app_id") appId: String,
                                  @Query("symbols") symbols: String?): Observable<RatesResponse> {

        val text = context.resources.openRawResource(R.raw.exchange_response)
                .bufferedReader().use { it.readText() }

        val response = gson.fromJson<RatesResponse>(text, RatesResponse::class.java)



        response.rates?.let {
            val keys = it.keys
            for (k in keys) {
                it[k] = (it[k] ?: 1.0) * Random.nextDouble(1.0, 1.2)
            }
        }

        return Observable.just(response).delay(1, TimeUnit.SECONDS)
    }
}